<?php

// Constant	
define('PXIsProd', FALSE);

// Globals
$GLOBALS['base_url'] = 'http://' . $_SERVER['HTTP_HOST'];
$GLOBALS['theme_path'] = 'theme';

// Core theme templates
$themes['html'] = array(
	'variables' => array(),
	'template' => 'html'
);
$themes['page'] = array(
	'variables' => array(),
	'template' => 'page'
);
$themes['page_front'] = array(
	'variables' => array(),
	'template' => 'page--front'
);
$themes['image_style'] = array(
	'variables' => array(
		'style_name' => NULL,
		'path' => NULL,
		'attributes' => array()
	)
);

function drupal_lookup_path () {
	return '';
}

function current_path () {
	return '';
}

function theme_render_template ($template_file, $variables) {
  // Extract the variables to a local namespace
  extract($variables, EXTR_SKIP);
	// Start output buffering
  ob_start();
	// Include the template file
  include $template_file;
	// End buffering and return its contents
  return ob_get_clean();
}

function theme ($hook, $variables = array()) {
	global $themes;
	if (isset($themes[$hook])) {
		$theme = $themes[$hook];
		$variables += $theme['variables'];
		if ($hook == 'image_style') {
			$output = theme_render_image($variables);
		}
		else {
			$theme_template = './theme/templates/' . $theme['template'] . '.tpl.php';
			$output = theme_render_template($theme_template, $variables);
		}
	}
	if (!isset($output)) {
		$output = '<code><< Erreur de templating => theme(' . $hook . ') >></code>';
	}
	return $output;
}

function drupal_parse_info_file($filename) {
  $info = &drupal_static(__FUNCTION__, array());

  if (!isset($info[$filename])) {
    if (!file_exists($filename)) {
      $info[$filename] = array();
    }
    else {
      $data = file_get_contents($filename);
      $info[$filename] = drupal_parse_info_format($data);
    }
  }
  return $info[$filename];
}

function drupal_parse_info_format($data) {
  $info = array();
	$constants = get_defined_constants();

	if (preg_match_all('
		@^\s*													 # Start at the beginning of a line, ignoring leading whitespace
		((?:
			[^=;\[\]]|										# Key names cannot contain equal signs, semi-colons or square brackets,
			\[[^\[\]]*\]									# unless they are balanced and not nested
		)+?)
		\s*=\s*												 # Key/value pairs are separated by equal signs (ignoring white-space)
		(?:
			("(?:[^"]|(?<=\\\\)")*")|		 # Double-quoted string, which may contain slash-escaped quotes/slashes
			(\'(?:[^\']|(?<=\\\\)\')*\')| # Single-quoted string, which may contain slash-escaped quotes/slashes
			([^\r\n]*?)									 # Non-quoted string
		)\s*$													 # Stop at the next end of a line, ignoring trailing whitespace
		@msx', $data, $matches, PREG_SET_ORDER)) {
		foreach ($matches as $match) {
			// Fetch the key and value string.
			$i = 0;
			foreach (array('key', 'value1', 'value2', 'value3') as $var) {
				$$var = isset($match[++$i]) ? $match[$i] : '';
			}
			$value = stripslashes(substr($value1, 1, -1)) . stripslashes(substr($value2, 1, -1)) . $value3;

			// Parse array syntax.
			$keys = preg_split('/\]?\[/', rtrim($key, ']'));
			$last = array_pop($keys);
			$parent = &$info;

			// Create nested arrays.
			foreach ($keys as $key) {
				if ($key == '') {
					$key = count($parent);
				}
				if (!isset($parent[$key]) || !is_array($parent[$key])) {
					$parent[$key] = array();
				}
				$parent = &$parent[$key];
			}

			// Handle PHP constants.
			if (isset($constants[$value])) {
				$value = $constants[$value];
			}

			// Insert actual value.
			if ($last == '') {
				$last = count($parent);
			}
			$parent[$last] = $value;
		}
	}

	return $info;
}

function &drupal_static($name, $default_value = NULL, $reset = FALSE) {
  static $data = array(), $default = array();
  // First check if dealing with a previously defined static variable.
  if (isset($data[$name]) || array_key_exists($name, $data)) {
    // Non-NULL $name and both $data[$name] and $default[$name] statics exist.
    if ($reset) {
      // Reset pre-existing static variable to its default value.
      $data[$name] = $default[$name];
    }
    return $data[$name];
  }
  // Neither $data[$name] nor $default[$name] static variables exist.
  if (isset($name)) {
    if ($reset) {
      // Reset was called before a default is set and yet a variable must be
      // returned.
      return $data;
    }
    // First call with new non-NULL $name. Initialize a new static variable.
    $default[$name] = $data[$name] = $default_value;
    return $data[$name];
  }
  // Reset all: ($name == NULL). This needs to be done one at a time so that
  // references returned by earlier invocations of drupal_static() also get
  // reset.
  foreach ($default as $name => $value) {
    $data[$name] = $value;
  }
  // As the function returns a reference, the return should always be a
  // variable.
  return $data;
}

/**
 * Sharing variables between preprocess functions
 */
function _palpix_toolbox_var ($var_name = NULL, $new_val = NULL) {
  $vars = &drupal_static(__FUNCTION__, array());
  if (!$var_name && !$new_val) {
  	return $vars;
  }
  if ($new_val != NULL) {
    $vars[$var_name] = $new_val;
  }
  return isset($vars[$var_name]) ? $vars[$var_name] : NULL;
}

function variable_get ($var_name, $default = NULL) {
	switch ($var_name) {
		case 'css_js_query_string' :
			return time();
			break;
		case 'site_name' :
			return _SITENAME_;
			break;
	}
}

function drupal_add_html_head () {}

// retourne l'url d'un template avec une querystring en option
function url ($page_id = NULL, $options = array()) {
	$params = $_GET;
	unset($params['page']);
	$defaults = empty($params) ? array() : $params;
	$config = array();
	if (gettype($options) != "array") {
		$config = NULL;
	}
	else if (is_array($defaults)) {
		$config = array_merge($defaults, $options);
	}
	else {
		$config = $options;
	}
	
	$querystring = '';
	if ($config != NULL && is_array($config)) {
		if (isset($config['logged']) && !$config['logged']) {
			unset($config['logged']);
		}
		if (!empty($config)) {
			$querystring = '&' . http_build_query($config);
		}
	}

	$path = 'page=' . (($page_id == NULL) ? $_GET['page'] : $page_id);
	$url = $GLOBALS['base_url'] . $GLOBALS['base_path'] . '?' . $path . $querystring;
	return $url;
}

function drupal_is_front_page () {
	$bool = FALSE;
	$current_page = get_current_page();
	if ($current_page && $current_page['id'] == 'homepage') {
		$bool = TRUE;
	}
	return $bool;
}

function arg () {}
function menu_get_object () {}

function user_is_logged_in () {
	$bool = FALSE;
	if (isset($_GET['logged']) && $_GET['logged'] == '1') {
		$bool = TRUE;
	}
	return $bool;
}

function _palpix_toolbox_clean_string ($string, $length = 80) {
	static $_search = array(' ');
	static $_replace = array(' ');
	if (is_string($string)) {
		return substr(trim(strip_tags(str_replace($_search, $_replace, $string))), 0, $length);
	}
}

function lorem ($type = 'text', $index = -1) {
	global $lorem;
	if ($index == -1 && isset($lorem[$type])) {
		$index = rand(0, count($lorem[$type])-1);
	}
	if (isset($lorem[$type][$index])) {
		$texte = $lorem[$type][$index];
	}
	else {
		$texte = '<code><< Erreur faux texte : lorem(\'' . $type . '\',\'' . $index . '\') >></code>';
	}
	return $texte;
}

function theme_render_image ($vars) {
	global $image_styles;
	$output = '';
	$style = NULL;
	$width = NULL;
	$height = NULL;
	$files_dir = 'files';
	$resize = FALSE;
	
	// Compression (qualité)
	$q = 100;
	
	// Récupère une image aléatoire si pas de path
	if ($vars['path'] == NULL) {
		$filelist = array();
		if ($dir = @opendir($files_dir . '/')) {
			while (($file = readdir($dir)) !== false) {
				if (!in_array($file, array('..', '.', '.DS_Store', '.gitignore', '.svn', 'cache', 'timthumb.php'))) {
					$filelist[] = $file;
				}
			}
			closedir($dir);
		}
		$vars['path'] = $filelist[rand(0, count($filelist)-1)];
	}
	
	$source = $files_dir . '/' . $vars['path'];
	$src = $GLOBALS['base_url'] . $GLOBALS['base_path'] . $files_dir . '/timthumb.php?src=' . $GLOBALS['base_path'] . $source . '&q=' . $q;
	
	// Styles
	if (isset($image_styles[$vars['style_name']])) {
		$style = $image_styles[$vars['style_name']];
		if (isset($style['width'])) {
			$src .= '&w=' . $style['width'];
			$resize = TRUE;
		}
		if (isset($style['height'])) {
			$src .= '&h=' . $style['height'];
			$resize = TRUE;
		}
	}
	
	if (!$resize) {
		$src = $source;
	}
	
	if (isset($vars['height'])) {
		$height = $vars['height'];
	}
	if (isset($vars['width'])) {
		$width = $vars['width'];
	}
	
	$image_infos = getimagesize($src);
	if ($width == NULL) {
		$width = $image_infos[0];
	}
	if ($height == NULL) {
		$height = $image_infos[1];
	}
	
	// Atrtributes
	$alt = (isset($vars['alt']) && !empty($vars['alt'])) ? ' alt="' . $vars['alt'] . '"' : '';
	$title = (isset($vars['title']) && !empty($vars['title'])) ? ' title="' . $vars['title'] . '"' : '';
	$attrs = '';
	if (isset($vars['attributes']) && !empty($vars['attributes'])) {
		foreach ($vars['attributes'] as $attr_name => $attr_val) {
			$attrs .= ' ' . $attr_name;
			if (gettype($attr_val) == 'string') {
				$attrs .= '="' . $attr_val . '"';
			}
		}
	}
	
	// Render element
	$output = '<img src="' . $src . '" width=' . $width . ' height="' . $height . '"' . $alt . $title . $attrs . '>';
	return $output;
}

function file_create_url ($path) {
	return $GLOBALS['base_path'] . 'files/' . $path;
}