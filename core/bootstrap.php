<?php

// Dependencies
require_once 'dd.php'; // Debugger helper
require_once 'config.php';
require_once 'drupal.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/theme/template.php';

// Init
function init () {
	$current_page = get_current_page();
	$node = preprocess_node($current_page);
	$page = preprocess_page($node, $current_page);
	$html = preprocess_html($page, $current_page);
	print $html;
}

function preprocess_html ($page, $current) {
	// On récupère les données du theme .info
	$info = drupal_parse_info_file('./theme/theme.info');
	
	// Variables par défaut
	$vars = array(
		'head' => '',
		'head_title' => render_title(),
		'flush_query' => time(),
		'directory' => 'theme',
		'preprocess_js' => TRUE,
		'styles' => render_styles($info['stylesheets']),
		'page_top' => '',
		'classes' => '',
		'scripts' => render_scripts($info['scripts']),
		'page_bottom' => ''
	);
	
	// On passe dans la fonction de preprocess
	THEME_preprocess_html($vars, NULL);
	
	$vars['page'] = $page;
	
	// Templating
	return theme ('html', $vars) . '<!-- html -->';
}

function preprocess_page ($node, $current) {
	global $themes;
	
	// Gestion du template de theme
	$page_theme = 'page';
	if (isset($current['suggestions']['page']) && valid_suggestion($current['suggestions']['page'])) {
		$page_theme = $current['suggestions']['page'];
	}

	// Variables par défaut
	$vars = array(
		'directory' => 'theme'
	);
	
	// On passe dans la fonction de preprocess
	THEME_preprocess_page($vars, NULL);
	$vars['node'] = $node;

	// Templating
	return theme($page_theme, $vars) . '<!-- ' . $page_theme . ' -->';
}

function preprocess_node ($current = NULL) {
	$page_id = $current['id'];
	
	$vars = array(
		'node_url' => $_SERVER['REQUEST_URI']
	);

	$page_path = NULL;
	if ($page_id != NULL) {
		$page_path = './pages/' . $page_id . '.php';
		if (file_exists($page_path)) {
			$out = theme_render_template ($page_path, $vars);
		}
	}
	if (!isset($out))  {
		$out = pages_render_list($page_id);
	}

	return $out . '<!-- ' . $page_id . ' -->';
}

function get_current_page () {
	global $pages;
	$out = NULL;
	if (isset($_GET['page']) && isset($pages[$_GET['page']])) {
		$page_id = $_GET['page'];
		$out = $pages[$page_id];
		$out['id'] = $page_id;
	}
	return $out;
}

function valid_suggestion ($suggestion) {
	global $themes;
	$bool = FALSE;
	foreach ($themes as $sug => $data) {
		if ($sug == $suggestion) {
			$bool = TRUE;
		}
	}
	return $bool;
}

function render_scripts ($scripts) {
	$out = array();
	foreach ($scripts as $script) {
		$out[] = '<script src="' . $GLOBALS['base_url'] . $GLOBALS['base_path'] . 'theme/' . $script . '?' . _CACHE_ . '"></script>';
	}
	return implode("\n", $out);
}

function render_styles ($stylesheets) {
	$out = array();
	foreach ($stylesheets as $media => $paths) {
		foreach ($paths as $path) {
			$out[] = '<link rel="stylesheet" href="' . $GLOBALS['base_url'] . $GLOBALS['base_path'] . 'theme/' . $path . '?' . _CACHE_ . '" media="' . $media . '">';
		}
	}
	return implode("\n", $out);
}

function render_title () {
	$out = array();
	$current_page = get_current_page();
	if ($current_page != NULL) {
		$out[] = $current_page['title'];
	}
	$out[] = _SITENAME_;
	return implode(' - ', $out);
}

function pages_render_list ($page_id = NULL) {
	global $pages;
	$out = '<ul class="pages-list">';
	foreach ($pages as $id => $page) {
		$out .= '<li><a href="' . url($id) . '">' . $page['title'] . '</a></li>';
	}
	$out .= '</ul>';
	return $out;
}

function example ($title, $code, $desc = '') {
	return '<div class="px-example">
	<div class="px-example_header">
		<div class="px-example_title">' . $title . '</div>'
		. (!empty($desc) ? '<div class="px-example_desc">' . $desc . '</div>' : '') . '
	</div>
	<div class="px-example_render">
		' . $code . '
	</div>
	<div class="px-example_code">
		<pre>' . htmlentities($code) . '</pre>
	</div>
</div>';
}