<?php

$GLOBALS['base_path'] = '/';

define('_SITENAME_', 'Nom du site');
define('_CACHE_', time());

$pages = array(
	'base' => array(
		'title' => 'Base'
	)
	/*,
	'homepage' => array(
		'title' => 'Homepage',
		'suggestions' => array(
			'page' => 'page_front'
		)
	)*/
);

$themes = array(
	'tag_ga' => array(
		'variables' => array(),
		'template' => 'tag-ga'
	)
);

$lorem = array(
	'text' => array(
		'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
	)
);

$image_styles = array(
	'carre_200' => array(
		'width' => 200,
		'height' => 200
	)
);