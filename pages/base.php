<link rel="stylesheet" href="<?php print $GLOBALS['base_url'] . $GLOBALS['base_path']; ?>theme/css/masters/socle.css?<?php print _CACHE_; ?>">
<div class="trunk">
<?php print example('Buttons elements', '
	<a href="#" class="btn">Anchor button</a>
	<button class="btn">Button</button>
	<input type="submit" class="btn" value="Submit button">
'); ?>

<?php print example('Buttons disabled', '
	<div class="btn btn--disabled">Anchor button</div>
	<button class="btn btn--disabled" disabled>Button</button>
	<input type="submit" class="btn btn--disabled" value="Submit button" disabled>
'); ?>

<?php print example('Buttons sizes', '
	<div class="btn btn--s">Button S</div>
	<div class="btn">Button</div>
	<div class="btn btn--l">Button L</div>
	<div class="btn btn--xl">Button XL</div>
'); ?>

<?php print example('Buttons themes', '
	<a href="#" class="btn btn--primary">Button primary</a>
	<div class="btn btn--primary btn--disabled">Button primary disbaled</div><br><br>
	<a href="#" class="btn btn--secondary">Button secondary</a>
	<div class="btn btn--secondary btn--disabled">Button secondary disbaled</div>
'); ?>

<?php print example('Buttons block mode', '
	<div class="btn btn--block">Button</div>
'); ?>
</div>