# Socle intégration

## A propos

Ce socle fournis un environnement de travail et les outils de développement permettant de débuter la phase d'intégration d'un nouveau site. Il contient un ensemble de bonnes pratiques pour optimiser les performances front-end.

L'objectif est de pouvoir présenter l'ensemble des pages HTML correspondant aux maquettes graphiques et de faciliter le branchement dans Drupal.

---

## Prérequis

- [CLI](http://fr.wikipedia.org/wiki/Interface_en_ligne_de_commande) (Teminal, iTerm, ...)
- [Ruby](https://www.ruby-lang.org/fr/) pour faire tourner [Sass](http://sass-lang.com/) (Ruby est déjà installé sur Mac)
- [Compass](http://compass-style.org/) Framework Sass
- Server local avec Apache et le module SSI d'activé sur les fichiers JS ([MAMP](http://www.mamp.info/en/), [WAMP](http://www.wampserver.com/))

---

## Projet

Exemple de dossier "projet" :
```
[nom_projet]
 |_ doc
 |_ design
 |_ integration
```

---

## Installation

Déposer les fichiers dans le dossier `integration` du du projet.

En fonction de la configuration du server local, il faudra peut-être changer le chemin de la racine du site.

Il faut alors modifier la variable `$GLOBALS['base_path']` dans `core/config.php`.


---

## Organisation des fichiers

### Structure
```
 |_ .htaccess
 |_ index.php
 |_ core/
 |_ files/
 |_ pages/
 |_ theme/
```

### .htaccess
Configuration server inspirée du [htaccess de HTML5Boilerplate](https://github.com/h5bp/html5-boilerplate/blob/master/dist/.htaccess)

### index.php
Page d'accueil du socle. Affiche la liste les pages disponibles qui est mise à jour à partir des pages déclarées dans le tableau `$pages` (`core/config.php`)

### core/
Mini framework PHP : bootstrap, constantes, fonctions d'aides, templates rendering, génération de faux contenu...

### files/
Images de contenu. S'assurer que le dossier `files/cache/` possède les droits en écriture.

### pages/
Liste des fichiers contenant le HTML des différentes pages à intégrer.

### theme/
Répertoire reprenant la structure d'un thème Drupal. Ce dossier devra être copié dans l'install Drupal lors du branchement du site.

---

## Méthodologie

### 1. Analyse des maquettes
1. A partir des maquettes, définir les variables de base (taille texte, couleurs principales…)
1. Repérer et classer les éléments spécifiques du design (header, footer, ...) et les modules qui seront réutilisables (boutons, blocs similaires + variations…)
1. Pour chaque élément, définir un nom de base qui sera ensuite utilisé comme class HTML (exemple: `btn`)
1. Définir un nom pour chaque variation de cet élément (exemple `btn--default`)
1. Pour faciliter la compréhension visuelle, il est possible d'exporter une image par élément. Exemple dans le Finder Mac : ![Buttons](assets/buttons.png)

### 2. Intégration des éléments de base

1. Pour chaque élément, créer le fichier `.scss` associé dans le dossier `theme/css/scss/base/` ou `theme/css/scss/modules/` en fonction de son utilisation. Exemple : `theme/css/scss/base/_bouton.scss`
1. Ajouter une référence à ce fichier dans le "master" qui sera compilé `theme/css/scss/masters/_masters.scss`
1. Dans la page `pages/base.php`, créer un nouveau bloc qui contiendra le HTML de l'élément. On peut écrire directement le HTML :
```
<?=demo_element('Button default', '<a href="" class="btn btn--default">Default button</a>')?>
```
ou utiliser un template :
```
<?=demo_element('Button default', theme('button', array('style' => 'default')))?>
```
Au final on obtient une page de référence des modules ([patterns librairy](http://codepen.io/patterns/)). [Exemple](http://www.palpix.com/interne/legibase/integration/pages/000_base.php).

### 3. Intégration des différentes pages
1. Créer un nouveau fichier dans le dossiers `pages/` pour chaque gabarit
2. Commencer par la structure principale de la page (exemple : colonnes principale et secondaire)
3. Ensuite intégrer les différents blocs spécifiques de la page

---

## Fonctions PHP

###  - `lorem(type, index)`

**Retour**

{string} Texte de faux contenu.

**Arguments** 

- `type` : {string} type de contenu ('text' par défaut)
- `index` : {number} index de l'item du tableau du type demandé ('-1' par défaut = index aléatoire)


### - `demo_element(name, code, show_code, desc)`

**Retour**

{string} Code HTML de présentation d'un élément de base.

**Arguments**

- `name` : {string} nom de l'élément
- `code` : {string} code HTML de l'élément
- `show_code` : {bool} détermine si le code HTML doit être visible par défaut
- `desc` : {string} description de l'élément

**Exemple**

```
<?=demo_element('Button', '<a href="" class="btn btn--default">Default button</a>')?>
```
sortie HTML : 
```
<div class="demo-wrapper demo-button">
	<span id="demo-button" class="demo-anchor anchor"></span>
	<div class="demo">
		<div class="demo_inner">
			<a href="#demo-button" class="demo__title">Buttons</a>
			<div class="demo__desc"></div>
			<div class="demo__render"><a href="" class="btn btn--default">Default button</a></div>
			<div class="demo__code"><code>&lt;a href="" class="btn btn--default"&gt;Default button&lt;/a&gt;</code></div>
		</div>
	</div>
</div>
```
affichage :

![demo](assets/demo.png)

---

## Styles

### Détail du dossier `theme/css`
```
 |_ masters/
 |_ scss/
   |_ base/      : reset CSS, typo, éléments de base...
   |_ config/    : déclaration des variables Sass
   |_ libraries/ : styles importés depuis une source externe (Drupal,…)
   |_ masters/   : fichiers destinés à être compilés
   |_ mixins/    : fonctions Sass
   |_ modules/   : liste des modules intégrés (header, navigation…). C'est le principal dossier de travail lors de l'intégration.
```

### Compilation

La configuration Compass se trouve dans le fichier `theme/config.rb`. Pour lancer la compilation, il suffit d'ouvrir une fenêtre du Terminal, de se déplacer dans le dossier `theme/` et de lancer cette commande :
```
$ compass compile
```
Pour éviter de lancer cette commande à chaque fois, on peut utiliser la suivante :
```
$ compass watch
```
Cette commande permet de "surveiller" les fichiers et de lancer la compilation à chaque modification.

Les feuilles de style sont générées dans le dossier `theme/css/masters/` en prenant comme référence le dossier `theme/css/scss/masters/`. Exemples :

`theme/css/scss/masters/screen.scss` => `theme/css/masters/screen.css`

`theme/css/scss/masters/print.scss` => `theme/css/masters/print.css`

---

## Aide / FAQ

### Ajouter du faux contenu
Dans `core/config.php`, ajouter un item au tableau `$lorem` : soit un nouveau type de contenu (title, name…), soit un nouveau contenu texte. Exemple :
```
<?php
$lorem = array(
	'firstname' => array('Bob', 'John')
);
?>
```
Utilisation :
```
<?php
print lorem('firstname', 1); // => John
?>
```

### Templating
Le système de templating est basé sur celui de Drupal. Voici les étapes pour créer un nouveau template :

1. Ajouter un item au tableau `$themes` dans `core/config.php`, à la manière de Drupal. Exemple :
```
'user_bloc' => array(
		'variables' => array(
			'name' => 'Toto',
			'age' => 20
		),
		'template' => 'user-bloc'
)
```
- `user_bloc` : identifiant du template
- `variables` : valeurs par défaut des variables qui seront injectées dans le fichier de template
- `user-bloc` : nom du ficher de template
2. Dans `theme/templates/` créer le fichier associé (`user-bloc.tpl.php`) contenant le HTML du template en utilisant les variables passées en paramètres. Exemple :
```
<div class="user-bloc">
		<div>Name: <?=$name?></div>
		<div>Age: <?=$age?></div>
</div>
```

3. Utilisation :
```
<?php
$myage = 30;
print theme('user_bloc', array('age' => $myage));
?>
```
Ce qui affichera :
```
<div class="user-bloc">
		<div>Name: Toto</div>
		<div>Age: 30</div>
</div>
```