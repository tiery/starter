<?php

/**
 * Implement theme_preprocess_html
 */
function THEME_preprocess_html (&$vars, $hook) {
	// Le module meta tags ne prends pas en compte pour la HP. On le force
	if (drupal_is_front_page()) {
	  $vars = _THEME_set_metatags_hp($vars);
	}

	// Gestion du cache JS et CSS
	$vars['flush_query'] = variable_get('css_js_query_string', '0');

	// AJoute le ta ggoogle analytics
	$vars['tag_ga'] = theme('tag_ga', array('id' => 'UA-XXXXXXXX-Y'));
}

/**
 * Implement theme_preprocess_page
 */
function THEME_preprocess_page (&$vars, $hook) {

	// si on affiche un node
	if (isset($vars['node'])) {
		// on récupère le title
		$title = $vars['node']->title;
	}
	// si on affiche une page taxo
	else if (arg(0) == 'taxonomy' && arg(1) == 'term' && is_numeric(arg(2))) {
		$tid = arg(2);
		$taxo = taxonomy_term_load($tid);
		$vars['vid'] = $taxo->vid;
	}

	// récupère tous les parents de la taxo courante
	if (isset($tid) && $tid>0) {
		if (!isset($taxo) || $taxo->vid != 7) {
			$parents = array_reverse(taxonomy_get_parents_all($tid));
			foreach ($parents as $i => $parent) {
				if ($i==0 && $parent->vid == 3) {
					//on force le lien vers la cover rubrique au lieu de la page de liste
					module_load_include('inc', 'pathauto', 'pathauto');
					$breadcrumb[] = array($parent->name, pathauto_cleanstring($parent->name).'/');
				}
				else {
					// on rempli le breadcrumb
					$breadcrumb[] = array($parent->name, 'taxonomy/term/' . $parent->tid);
				}
			}
		}
		else {
			if (isset($taxo->field_thematique['und'][0]['target_id'])) {
				$rubrique = taxonomy_term_load($taxo->field_thematique['und'][0]['target_id']);
				$breadcrumb[] = array($rubrique->name, 'taxonomy/term/'.$rubrique->tid);
				$breadcrumb[] = array($taxo->name);
			}
		}
	}

	// si on a récupéré le titre d'un node
	if (isset($title)) {
		// on ajoute le title à la fin du breadcrumb
		$breadcrumb[] = array($title, NULL);
	}
	
	// On génère le breadcrumb
	if (isset($breadcrumb)) {
		_palpix_set_breadcrumb($breadcrumb);
	}
}

/**
 * Implement theme_preprocess_node
 */
function THEME_preprocess_node(&$vars) {
	//AH : variable $submitted_date retourne la date de creation et de mise à jour
	$vars['submitted_date'] = _watchees_format_date_article($vars['node']->created, $vars['node']->changed);
	$vars['pageUrl'] = $GLOBALS['base_url'] . $vars['node_url'];
}

function THEME_html_head_alter(&$head_elements) {
	// Set the first media field as an og:image
	if (arg(0) == 'node' && is_numeric(arg(1))) {
		$node = node_load(arg(1));
		if ($node && isset($node->field_image['und'][0]['nid'])) {
			$nid_image = $node->field_image['und'][0]['nid'];
			if ($nid_image > 0) {
				$uri = db_query_range("SELECT fm.uri FROM {file_managed} fm INNER JOIN {field_data_field_mediatheque_image} fdfmi ON fm.fid = fdfmi.field_mediatheque_image_fid WHERE fdfmi.entity_id = :nid", 0, 1, array(':nid' => $nid_image))->fetchField();
				$head_elements['metatag_og:image']['#tag'] = 'meta';
				$head_elements['metatag_og:image']['#value'] = file_create_url($uri);
		  }
		}
	}
	
	// AH pour corriger le double canonical sur les nodes (on garde celui de global redirect)
	foreach (preg_grep('/^drupal_add_html_head_link:canonical:</', array_keys($head_elements)) as $key) {
		unset($head_elements[$key]);
	}
}

/**
 * breadcrumb
 */
function THEME_breadcrumb($vars) {
  $breadcrumb = $vars['breadcrumb'];
  $output = '';
  if (!empty($breadcrumb) && count($breadcrumb) > 1) {
  	$output = '<ul class="breadcrumb"><!--';
    foreach($breadcrumb as $item) {
    	$output .= '--><li class="breadcrumb_item">' . $item . '</li><!--';
    }
    $output .= '--></ul>';
  }
  return $output;
}

// theme_textarea
function THEME_textarea($element)  {
	// TL Désactive le resize des textarea 
	$element['element']['#resizable'] = false ;
	return theme_textarea($element) ;
}

/**
 * Returns HTML for a fieldset form element and its children.
 * => Replace <legend> by <div class="legend">, because legend element is "buggy"
 */
function THEME_fieldset($variables) {
	$element = $variables['element'];
	element_set_attributes($element, array('id'));
	_form_set_class($element, array('form-wrapper'));
	
	$output = '<fieldset' . drupal_attributes($element['#attributes']) . '>';
	if (!empty($element['#title'])) {
		// Always wrap fieldset legends in a SPAN for CSS positioning.
		$output .= '<div class="legend"><span class="fieldset-legend">' . $element['#title'] . '</span></div>';
	}
	$output .= '<div class="fieldset-wrapper">';
	if (!empty($element['#description'])) {
		$output .= '<div class="fieldset-description">' . $element['#description'] . '</div>';
	}
	$output .= $element['#children'];
	if (isset($element['#value'])) {
		$output .= $element['#value'];
	}
	$output .= '</div>';
	$output .= "</fieldset>\n";
	return $output;
}

/**
 * Returns HTML for a form element label and required marker.
 * => add "has-placeholder" class if needed
 */
function THEME_form_element_label($variables) {
	$element = $variables['element'];
	// This is also used in the installer, pre-database setup.
	$t = get_t();
	
	// If title and required marker are both empty, output no label.
	if ((!isset($element['#title']) || $element['#title'] === '') && empty($element['#required'])) {
		return '';
	}
	
	// If the element is required, a required marker is appended to the label.
	$required = !empty($element['#required']) ? theme('form_required_marker', array('element' => $element)) : '';
	
	$title = filter_xss_admin($element['#title']);
	
	$attributes = array();
	$attributes['class'] = '';
	// Style the label as class option to display inline with the element.
	if ($element['#title_display'] == 'after') {
		$attributes['class'] = 'option';
	}
	// Show label only to screen readers to avoid disruption in visual flows.
	elseif ($element['#title_display'] == 'invisible') {
		$attributes['class'] = 'element-invisible';
	}
	
	// Si l'élément correspondant possède un placeholder, on l'indique au label
	if (isset($element['#attributes']['placeholder']) && $element['#attributes']['placeholder'] != '') {
		$attributes['class'] .= ' has-placeholder';
	}
	
	$attributes['class'] .= ' label-' . $variables['element']['#type'];
	
	if (!empty($element['#id'])) {
		$attributes['for'] = $element['#id'];
	}
	
	// The leading whitespace helps visually separate fields from inline labels.
	return ' <label' . drupal_attributes($attributes) . '>' . $t('!title !required', array('!title' => $title, '!required' => $required)) . "</label>\n";
}

/**
 * hook_pager
 */
function THEME_pager($variables) {
	$tags = $variables['tags'];
	$element = $variables['element'];
	$parameters = $variables['parameters'];
	$quantity = $variables['quantity'];
	global $pager_page_array, $pager_total;

	// Calculate various markers within this pager piece:
	// Middle is used to "center" pages around the current page.
	$pager_middle = ceil($quantity / 2);
	// current is the page we are currently paged to
	$pager_current = $pager_page_array[$element] + 1;
	// first is the first page listed by this pager piece (re quantity)
	$pager_first = $pager_current - $pager_middle + 1;
	// last is the last page listed by this pager piece (re quantity)
	$pager_last = $pager_current + $quantity - $pager_middle;
	// max is the maximum page number
	$pager_max = $pager_total[$element];

	// End of marker calculations.
	
	// Prepare for generation loop.
	$i = $pager_first;
	if ($pager_total[$element] > 1) {
		// When there is more than one page, create the pager list.
		if ($pager_max > 1) {
			// Now generate the actual pager piece.
			for ($i=1; $i <= $pager_max; $i++) {
			//On affiche les liens vers les pages modulo de 5, première et dernière page
			if (($i==1) || ($i%10==0) || ($i==$pager_max) || ( ($pager_current-$i)>-5 && ($pager_current-$i)<5)) {
				$classes = array();
				if($i == 1) $classes[]='first';
				if ($i < $pager_current) {
					$items[] = array(
						'class' => $classes,
						'data' => theme('pager_previous', array('text' => $i, 'element' => $element, 'interval' => ($pager_current - $i), 'parameters' => $parameters)),
					);
				}
				if ($i == $pager_current) {
					$classes[]='active';
					$items[] = array(
						'class' => $classes,
						'data' => '<span>'.$i.'</span>',
					);
				}
				if ($i > $pager_current) {
					$items[] = array(
						'class' => $classes,
						'data' => theme('pager_next', array('text' => $i, 'element' => $element, 'interval' => ($i - $pager_current), 'parameters' => $parameters)),
					);
				}
				$elipse = true;
			}
			else if ($elipse == true) {
				$classes[]='sep';
				$items[] = array(
					'class' => $classes,
					'data' => '…',
				);
				$elipse = false;
			}
		}
	}
	$html = '';
	$html .= '<ul class="pagine">';
	
	if (is_array($items) && count($items) > 0) {
		foreach ($items as $item) {
			$html .= '<li' . drupal_attributes(array('class' => implode(' ', $item['class']))) . '>' . $item['data'] . '</li>';
		}
	}
	$html .= '</ul>';
	return $html;
	}
}

// Set les titles et open graph de la HP
function _THEME_set_metatags_hp ($vars){
	// Vars
	$title = variable_get('site_name');
	$description = variable_get('site_name');
	
	// Meta
	$meta_description = array(
		'#type' => 'html_tag',
		'#tag' => 'meta',
		'#attributes' => array(
			'name' => 'description',
			'content' =>  $description
		)
	);
	$meta_keywords = array(
		'#type' => 'html_tag',
		'#tag' => 'meta',
		'#attributes' => array(
			'name' => 'keywords',
			'content' =>  ''
		)
	);
	
	// Open Graph
	$og_title = array(
		'#tag' => 'meta',
		'#attributes' => array(
			'property' => 'og:title',
			'content' => $title,
		),
	);
	$og_description = array(
		'#tag' => 'meta',
		'#attributes' => array(
			'property' => 'og:description',
			'content' => $description
		),
	);
	$og_type = array(
		'#tag' => 'meta',
		'#attributes' => array(
			'property' => 'og:type',
			'content' => 'Website',
		),
	);
	$og_image = array(
		'#tag' => 'meta',
		'#attributes' => array(
			'property' => 'og:image',
			'content' => $GLOBALS['base_url'] . $GLOBALS['base_path'] . $GLOBALS['theme_path'] . '/img/logo-og.jpg',
		),
	);
	$og_url = array(
		'#tag' => 'meta',
		'#attributes' => array(
			'property' => 'og:url',
			'content' => $GLOBALS['base_url'],
		),
	);
	
	$vars['head_title'] = $title;
	drupal_add_html_head( $og_title, 'og_title');
	drupal_add_html_head( $og_type, 'og_type');
	drupal_add_html_head( $og_description, 'og_description');
	drupal_add_html_head( $og_image, 'og_image');
	drupal_add_html_head( $og_url, 'og_url');
	drupal_add_html_head( $meta_description, 'meta_description' );
	drupal_add_html_head( $meta_keywords, 'meta_keywords' );
	return $vars;
}

function _THEME_unit_role_test ($role) {
	global $user;
	return (bool) (in_array($role, array_values($user->roles)));
}

function _THEME_user_roles ($roles = null) {
	if (!user_is_logged_in()) {
		return false;
	}
	
	$value = false;
	// Getter
	if ($roles == null) {
		$value = $user->roles;
	}
	
	// Tester
	elseif (_THEME_unit_role_test('Administrateur')) { // Si admin => open bar!
		$value = true;
	}
	elseif (is_string($roles)) { // 1 seul role à tester (string)
		$value = _THEME_unit_role_test($roles);
	}
	elseif (is_array($roles)) { // plusieurs roles (array)
		foreach ($roles as $role) {
			if (_THEME_unit_role_test($role)) {
				$value = true;
			}
		}
	}
	return $value;
}