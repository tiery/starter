/*! matchMedia() polyfill - Test a CSS media type/query in JS. Authors & copyright (c) 2012: Scott Jehl, Paul Irish, Nicholas Zakas. Dual MIT/BSD license */
(function (doc) {
	var docElem = doc.documentElement,
	    refNode = docElem.firstElementChild || docElem.firstChild,
	    fakeBody = doc.createElement('body'),
	    div = doc.createElement('div'),
	    polyfill;

	polyfill = (function(undefined) {
		var div = doc.createElement('div'),
			bool;
		div.id = "mq-test-1";
		div.style.cssText = "position:absolute;top:-100em";
		fakeBody.appendChild(div);
		return function (q) {
			// Media queries support => return matchMedia polyfill
			div.innerHTML = "&shy;<style media=\"" + q + "\">#mq-test-1{width: 83px;}</style>";
		    docElem.insertBefore(fakeBody, refNode);
		    bool = div.offsetWidth === 83;
		    docElem.removeChild(fakeBody);
		    return {
			    matches: bool,
			    media: q
			};
		};
	}());
	
	window['matchMedia'] = window['matchMedia'] || polyfill;

}(document));