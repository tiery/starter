/*
 * PX ad management
 */	
(function() {
	
	// Module declaration
	var moduleName = 'ad',
		logPrefix = 'px.' + moduleName;
		
	if (!PX.mod.test(moduleName, ['test'])) {
		return;
	}
	
	// Private properties
	var _stop = PX.config.stopAd;
	
	// Module definition
	PX[moduleName] = {
		
		// Write ad
		write: function (name) {
			var that = this,
				ad = PX.ads[name],
				context = this.isContext(name),
                regie;
            if (!context) {
	    		return;
	  		}
	  		else {
	  			regie = ad.regie || PX.config.adsRegie || false;
	  			if (regie && that[regie]) {
		  			this[regie](name);
	  			}
	  			else {
                    PXU.log(logPrefix + ' : Regie undefined!', 'error');
	  			}
	  		}
		},
		
		// Test context for ad to be displayed
		isContext: function (name) {
			var ad = PX.ads[name],
				breakpoint,
				destination,
				idDest,
				idDestRWD;
			
			if (_stop) {
				PXU.log(logPrefix + ' : Ads disabled!', 'warn');
				return;
			}
			
			// if ad doesn't exist
			if (!ad) {
				PXU.log(logPrefix + ' : is not declared in PX.ads - ' + name, 'error');
				return;
			}
			
			// is already loaded?
			if (ad.loaded) {
				PXU.log(logPrefix + ' : already loaded - ' + name);
				return;
			}
				
			// RWD
			if (ad.rwd && !PX.test('rwd', ad.rwd)) {
				PXU.log(logPrefix + ' : RWD doesn\'t match - ' + name);
				return;
			}
			
			// Check ad datas
			if (!ad.datas) {
				PXU.log(logPrefix + ' : datas error - ' + name, 'error');
				return;
			}
			
			// Destination element
			else {
				if (ad.sync) {
					return true;
				}
				idDest = 'ad-' + name + '-dst';
				
				// Check destination
				breakpoint = PX.test('rwd');
				idDestRWD = idDest + '-' + breakpoint;
				if (document.getElementById(idDestRWD)) {
					ad.destination = idDestRWD;
					return true;
				}
				else if (document.getElementById(idDest)) {
					ad.destination = idDest;
					return true;
				}
				else {
					PXU.log(logPrefix + ' : no destination element - ' + name, 'error');
					return;
				}
			}
			
			return;
		},
		
		createIframe: function (name) {
			var ad = PX.ads[name],
				iframe;
			if (document.getElementById('ad-' + name + '-iframe')) {
				return;
			}
			iframe = document.createElement('iframe');
			iframe.id = 'ad-' + name + '-iframe';
			iframe.width = ad.width;
			iframe.height = ad.height;
			iframe.className = 'ad-iframe';
			iframe.frameBorder = 0;
			document.getElementById('ad-' + name + '-wrap').appendChild(iframe);
			iframe.src = '../includes/ads/iframe.php?name=' + name;
			PXU.log(logPrefix + ' : iframe created - ' + name);
		},
		
		resizeIframe: function (name) {
			var ad = PX.ads[name],
				iframe = document.getElementById('ad-' + name + '-iframe'),
				iframeBody = iframe.contentWindow.document.body;
			iframe.width = iframeBody.clientWidth;
			iframe.height = iframeBody.clientHeight;
			PXU.log(logPrefix + ' : iframe resized - ' + name);
		},
		
		writeScript: function (name, ctx) {
			var ad = PX.ads[name],
				context = ctx || document;
			if (!ad.scriptUrl) {
				return;
			}
			context.write('<scr'+'ipt src="' + ad.scriptUrl + '"></scri'+'pt>');
			ad.loaded = true;
		},
		
		// Adtech
		'adtech': function (name) {
			var ad = PX.ads[name],
				scriptUrl = '';
			if (ad.loaded) {
				return;
			}
			if (!ad.datas.placementId || !ad.datas.sizeId) {
				PXU.log(logPrefix + ' : datas error - ' + name, 'error');
				return false;
			}
			if (window.adgroupid == undefined) {
				window.adgroupid = Math.round(Math.random() * 1000);
			}
			PX.ads[name].scriptUrl = 'http://adserver.adtech.de/addyn/3.0/' + ad.datas.networkId + '/' + ad.datas.placementId + '/0/' + ad.datas.sizeId + '/ADTECH;loc=100;target=_blank;key=key1+key2+key3+key4;grp=' + window.adgroupid + ';misc=' + new Date().getTime();
			if (ad.iframe) {
				this.createIframe(name);
			}
			else {
				this.writeScript(name);
			}
	        PXU.log(logPrefix + ' : loaded - ' + name);
		},
		
		shiftAd: function (name) {
			var ad = PX.ads[name];
			ad.destination.appendChild(ad.source);
			ad.shifted = true;
			PXU.log(logPrefix + ' : shifted - ' + name + ' -> ' + ad.destination.id);
		},
		
		// Shift each source to its own destination
		shift: function () {
			var that = this,				
				ad,
				name,
				source,
			    destination;
			for (name in PX.ads) {
				ad = PX.ads[name];
				if (ad.sync) {
					continue;
				}
				if (ad.require) {
					this.write(name);
				}
				destination = document.getElementById(ad.destination);
				if (destination) {
					source = document.getElementById('ad-' + name + '-src');			
					if (source) {
						ad.source = source;
						ad.destination = destination;
						this.shiftAd(name);
					}
				}
			}
		}
	
	};
})();