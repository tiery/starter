/*
 * Specific Touch device behavior
 */
(function(h,c,t) {
	if (PX.test(t)) {
		// Add FastClick
		window.addEventListener('load', function() {
			FastClick.attach(document.body);
		}, false);
	}
})(document.documentElement, 'className', 'touch');

/*
 * Plugins initialization
 */
(function($) {

	// Popin
	PX.plugins.register({
		name: 'popin',
		condition: function () {
			return PX.test('media', '(min-width: 768px)');
		},
		selector: '[data-popin]',
		callback: function () {
			PX.plugins.list.popin.init();
		}
	});
	
}(jQuery));