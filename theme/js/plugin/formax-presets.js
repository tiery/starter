(function($, pluginName) {

var presets = PX.plugins.list[pluginName].presets = {};

/**
 *
 * Ajout aux favoris
 *
 */
presets['tool_favorite'] = {
	onStatus: function (datas) {
		if (datas && datas.action && datas.action === 'remove') {
			$('.tool--favorite').removeClass('tool--active');
		}
		else {
			$('.tool--favorite').addClass('tool--active');
		}
	}
};

presets['favorite-new-dir'] = {
	handleMessages: false,
	onStatus: function (datas) {
		var that = this,
			newEl = '<div class="menu-perso_item menu-perso_item--n2">\
	<a href="' + datas.message.url + '" class="menu-perso_title menu-perso_title--n2">' + datas.message.name + '</a>\
</div>';
		that.$element.parents('.menu-perso_item--n2:eq(0)').before(newEl);
		that.$element.find('.toggler_closer').trigger('click');
	}
};

/**
 *
 * Alert
 *
 */
presets['tool_alert'] = {
	onStatus: function (datas) {
		if (datas && datas.action && datas.action === 'subscribe') {
			$('.tool--alert').addClass('tool--active');
		}
		else {
			$('.tool--alert').removeClass('tool--active');
		}
	}
};

/**
 *
 * Partage par mail
 *
 */
presets['tool_email'] = {
	ajaxConfig: {
		dataType: 'jsonp'
	}
};

}(jQuery, 'formax'));