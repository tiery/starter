(function ($) {

    'use strict';

    /*
     * Params
     */
    var pluginName = 'pxfontsizer',
        defaults = {
	        container: 'article-body',
	        disabledClass: 'disabled'
        },
        Plugin;

    /*
     * Plugin constructor
     */
    Plugin = function (element, options, i) {

        // Plugin exposed to window
        /* window[pluginName + i] = this; */

        // Main element
        this.$element = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');

        // Options
        var data = this.$element.data(pluginName),
        	elementOptions = {};
        if (typeof data === 'string') {
	        elementOptions.container = data;
        }
        else {
	        elementOptions = data;
        }
        var opts = $.extend({}, options, elementOptions); // element options -> plugin init options
        this.opts = $.extend({}, defaults, opts); // options -> default

        // DOM elements
        this.$container = $(document.getElementById(this.opts.container));
        this.$handlerPlus = this.$element.find('[data-action="plus"]');
        this.$handlerMinus = this.$element.find('[data-action="minus"]');
        
        // Properties
        this.size = 2;

        // Go!
        if (!this.$container.length) {
	        return;
        }
        this.init();
    };

    /*
     * Shortcut for Plugin object prototype
     */
    Plugin.fn = Plugin.prototype;

    /*
     * Initialization logic
     */
    Plugin.fn.init = function () {
        var that = this,
        	defaultSize;
		
		// Apply default font-size
		if (Modernizr.localstorage) {
			defaultSize = parseInt(localStorage.getItem(pluginName + that.opts.container, that.size), 10);
			if (defaultSize.toString() !== 'NaN') {
				that.process(defaultSize);
			}
		}
		
        // Events init
        that.events();
    };

    /*
     * Evenements
     */
    Plugin.fn.events = function () {
        var that = this,
        	action = '';
        that.$element.on('click', '[data-action]', function() {
        	action = $(this).data('action');
	        if (action == 'plus' && that.size < 4) {
		        that.process(that.size + 1);
	        }
	        else if (action == 'minus' && that.size > 0) {
		        that.process(that.size - 1);
	        }
        });
    };

    /*
     * Process
     */
    Plugin.fn.process = function (newSize) {
        var that = this;
        
        // Update container class
        that.$container
        	.removeClass('font-size-' + that.size)
        	.addClass('font-size-' + newSize);
        
        // Store new font-size
        that.size = newSize;
        if (Modernizr.localstorage) {
	        localStorage.setItem(pluginName + that.opts.container, that.size);
        }
        
        // Update handlers
        that.$handlerPlus.removeClass(that.opts.disabledClass);
        that.$handlerMinus.removeClass(that.opts.disabledClass);
        if (that.size === 4) {
			that.$handlerPlus.addClass(that.opts.disabledClass);
		}
	    else if (that.size === 0) {
			that.$handlerMinus.addClass(that.opts.disabledClass);
		}
    };

    // Plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function (i) {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
            }
        });
    };

}(jQuery));