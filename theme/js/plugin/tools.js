// Ajout email suplémentaire pour le partage
(function($) {
	$(document).on('click', '.tool-email--add-dest', function(){
		var $this = $(this),
			$new = $($this.prev()[0].outerHTML).val('');
		$this.before($new);
		$new.focus();
	});	
}(jQuery));

// Contrôle la taille de la typo
(function($) {
	var
		def = 0,
		min = -1,
		max = 2,
		$article_body = $(document.getElementById('article_body')).attr('data-size', def);
	
	$('.tool--enabled').on('click', '.tool_handler--fontsize', function(){
		var currentSize = parseInt($article_body.attr('data-size'), 10),
			newSize = currentSize + 1;
		if (newSize > max) {
			newSize = min;
		}
		$article_body.attr('data-size', newSize);
	});
}(jQuery));

// Mode lecture Zen
(function($) {
	var $html = $(document.documentElement).addClass('zen-mode--enabled'),
		$win = $(window),
		$hander = $('.tool_handler--zen'),
		active = false;
	
	$hander.on('click.zen', function() {
		active = !active;
		$html.toggleClass('zen-mode--active');
		$(this).parents('.tool:eq(0)').toggleClass('tool--active');
		$('.article_gear').trigger('update.flyer');
		if (active) {
			$win.on('keyup.zen', function(e){
				if (e.which === 27) {
					$hander.trigger('click.zen');
				}
			});
		}
		else {
			$win.off('keyup.zen');
		}
	});
}(jQuery));