<!--#include virtual="../libraries/jquery.textHighlighter.min.js"-->

(function ($) {

	'use strict';

	/*
	 * Params
	 */
	var pluginName = 'highlighter',
		defaults = {
			targetSelector: '#article_body',
		},
		keys = {
			targetActive: 'tool-' + pluginName + '_target--active',
			highlightedText: 'tool-' + pluginName + '_text'
		},
		Plugin;

	/*
	 * Plugin constructor
	 */
	Plugin = function (element, options, i) {

		// Plugin exposed to window
		/* window[pluginName + i] = this; */
		this.$element = $(element);

		// Options
		var elementOptions = this.$element.data(pluginName);
		var opts = $.extend({}, options, elementOptions); // element options -> plugin init options
		this.opts = $.extend({}, defaults, opts); // options -> default
		
		// DOM elements
		this.$target = $(this.opts.targetSelector);
		
		// Properties
		this.active = false;
		
		// Go!
		this.init();
	};

	/*
	 * Shortcut for Plugin object prototype
	 */
	Plugin.fn = Plugin.prototype;
	
	/*
	 * Initialization logic
	 */
	Plugin.fn.init = function () {
		var that = this;
		$.textHighlighter.createWrapper = function (options) {
			return $('<span></span>').addClass(keys.highlightedText).addClass(options.highlightedClass);
		};
		that.events();
	};

	/*
	 * Evenements
	 */
	Plugin.fn.events = function () {
		var that = this;
		that.$element.on('click', function(){
			var Highlighter;
			if (that.active) {
				that.$element.removeClass('tool--active');
				Highlighter = that.$target.removeClass(keys.targetActive).getHighlighter();
				//Highlighter.removeHighlights();
				Highlighter.destroy();
				that.active = false;
			}
			else {
				that.$element.addClass('tool--active');
				that.$target.textHighlighter().addClass(keys.targetActive);
				that.active = true;
			}
		});
	};

	// Plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function (options) {
		return this.each(function (i) {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
			}
		});
	};

}(jQuery));