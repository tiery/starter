(function ($) {

    'use strict';

    /*
     * Params
     */
    var pluginName = 'pxtabs',
        defaults = {
	        activeClass: 'active',
	        panelClass: 'onglets__panel',
	        title: 'Choix',
	        select: true
        },
        Plugin;

    /*
     * Plugin constructor
     */
    Plugin = function (element, options, i) {

        // Plugin exposed to window
        /* window[pluginName + i] = this; */

        // Main element
        this.$element = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');

        // Options
        var opts = $.extend({}, options, this.$element.data(pluginName)); // element options -> plugin init options
        this.opts = $.extend({}, defaults, opts); // options -> default

        // DOM elements
        this.$tabs = this.$element.find('li');
        this.$container = $(document.getElementById(this.opts.container));
        
        // Properties
        this.status = 'wait'; // wait | busy
        this.cache = {};

        // Go!
        if (!this.$container.length) {
	        return;
        }
        this.init();
    };

    /*
     * Shortcut for Plugin object prototype
     */
    Plugin.fn = Plugin.prototype;

    /*
     * Initialization logic
     */
    Plugin.fn.init = function () {
        var that = this;
        
        // Select fallback
        if (that.opts.select) {
	        that.createSelect();
        }

        // Events init
        that.events();
    };

    /*
     * Create select
     */
    Plugin.fn.createSelect = function () {
    	var that = this,
    		$el,
    		isActive,
    		select = ['<div class="onglets__select"><select name="' + pluginName + '-' + that.opts.container + '">'];
    	
    	select.push('<option value="-1">' + that.opts.title + '</option>');
    	
    	$.each(that.$tabs, function (i, el) {
    		$el = $(el);
    		isActive = $el.hasClass('active');
	    	select.push('<option value="' + i + '" ' + (isActive ? 'selected' : '')  + '>' + $el.text() + '</option>');
    	});
    	that.$select = $(select.join(''));
    	that.$element.after(that.$select);
    };

    /*
     * Evenements
     */
    Plugin.fn.events = function () {
        var that = this;
        that.$tabs.on('click', function (e) {
        	e.preventDefault();
        	var $this = $(this);
	        if (!$this.hasClass('active')) {
		        that.process($this);
	        }
        });
        if (that.opts.select) {
        	that.$select.find('select').on('change', function () {
        		var value = $(this).val(),
        			$handler;
        		if (value !== '-1') {
	        		$handler = that.$tabs.filter(':eq(' + value + ')');
	        		that.process($handler);
        		}
        	});
        }
    };

    /*
     * Process
     */
    Plugin.fn.process = function ($handler) {
        var that = this,
        	$req,
        	currentIndex,
        	index = 0,
        	path = '',
        	$panels,
        	content;
        if (that.status === 'busy') {
	        return;
        }
        that.status = 'busy';
        
        // Update tabs class / select
        currentIndex = that.$tabs.index(that.$tabs.filter('.' + that.opts.activeClass));
        that.$tabs.filter('.' + that.opts.activeClass).removeClass(that.opts.activeClass);
        $handler.addClass(that.opts.activeClass);
        
        // Get clicked tab index
        index = that.$tabs.index($handler);
        path = $handler.data('tab-path');
        
        if (that.opts.select) {
        	that.$select.find('select').val(index);
        }
        
        // Get async content
        if (path) {
        
        	// Caching current code before changing tab
        	that.cache[currentIndex] = that.$container.html();
	        
	        // Checking cache
	        if (that.cache[index]) {
	        	that.postProcess(that.cache[index]);
	        }
	        
	        // Process async request
	        else {
	        	that.$container.addClass('loading');
		        $req = $.ajax({
			        url: path,
			        cache: false
		        });
		        $req.done(function(datas){
			        that.postProcess(datas);
			        that.$container.removeClass('loading');
		        });
	        }
        }
        
        // sync content
        else {
        	$panels = $('> .' + that.opts.panelClass, that.$container);
	        $panels.filter('.active').removeClass(that.opts.activeClass);
	        $panels.filter(':eq(' + index + ')').addClass(that.opts.activeClass);
	        that.postProcess();
        }        
    };
    
    /*
     * Post process
     */
    Plugin.fn.postProcess = function (datas) {
    	var that = this;
    	if (datas || datas === '') {
	    	that.$container.html(datas);
    	}
		that.status = 'wait';
		$.publish('/' + pluginName + '/' + that.opts.container);
    };

    // Plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function (i) {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
            }
        });
    };

}(jQuery));