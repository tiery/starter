(function ($) {

	'use strict';

	/*
	 * Params
	 */
	var pluginName = 'tree',
		defaults = {},
		Plugin;

	/*
	 * Plugin constructor
	 */
	Plugin = function (element, options, i) {

		// Plugin exposed to window
		/* window[pluginName + i] = this; */

		// Main element
		this.$element = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');

		// Options
		var elementOptions = this.$element.data(pluginName);
		var opts = $.extend({}, options, elementOptions); // element options -> plugin init options
		this.opts = $.extend({}, defaults, opts); // options -> default

		// DOM elements
		// this.$inner	 = $('.une__inner', element);
		
		// Properties
		this.status = 'wait';
		this.toggleClass = 'tree_title--open';

		// Go!
		this.init();
	};

	/*
	 * Shortcut for Plugin object prototype
	 */
	Plugin.fn = Plugin.prototype;
	
	/*
	 * Initialization logic
	 */
	Plugin.fn.init = function () {
		var that = this;
		
		that.$element.find('div.tree_title').attr('tabindex', '0');
		
		// Events init
		that.events();
	};

	/*
	 * Evenements
	 */
	Plugin.fn.events = function () {
		var that = this;
		
		// User events
		that.$element.on('click', '.tree_title--has-children', function (e) {
			var handler = this;
			e.preventDefault();
			$(handler).toggleClass(that.toggleClass);
		});
		
		// Focus
		that.$element.on('focus', 'div.tree_title--has-children', function () {
			var $handler = $(this);
			$(window).on('keypress', function (e) {
				if (e.which === 13) {
					$handler.toggleClass(that.toggleClass);
				}
			});
			$handler.on('blur', function () {
				$(window).off('keypress');
			});
		});
	};
	
	/*
	 * Toggle view
	 */
	Plugin.fn.toggle = function (handler) {
		var that = this,
			$handler = $(handler),
			toggleClass = 'tree_title--open';
		$handler.toggleClass(toggleClass);
	}

	// Plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function (options) {
		return this.each(function (i) {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
			}
		});
	};

}(jQuery));