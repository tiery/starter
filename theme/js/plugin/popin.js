<!--#include virtual="../libraries/jquery-colorbox.js"-->
<!--#include virtual="popin-presets.js"-->

/*
 * Popin plugin
 * provide an api for modal popin
 * use colorbox plugin http://www.jacklmoore.com/colorbox
 *
 * $.fn.popin params mapped on $.fn.colorbox params but via presets
 * $.popin = $.colorbox
 * $.popin.presets
 */
 
(function ($) {

    'use strict';

    /*
     * Params
     */
    var pluginName = 'popin',
      $site = $(document.getElementById('site')),
      defaults = {
      	opacity: .9,
        initialWidth: 50,
        initialHeight: 50,
        close: 'Fermer',
        previous: '',
        next: '',
        current: '{current} / {total}',
        xhrError: 'Désolé une erreure est survenue.',
        maxWidth: '90%',
        maxHeight: '90%',
        onClosed: function () {
            $[pluginName].data = {
                element: null
            };
        }
      };
    
    /*
     * Plugin constructor
     */
    var Plugin = function (element, options, i) {
        
        // Expose Plugin to window
        window[pluginName + i] = this;
        
        // Main element
        this.element  = element;
        this.$element = $(element).addClass(pluginName.toLowerCase() + '-init');
        
        // options
        var elementOptions = this.$element.data(pluginName);
        if (typeof elementOptions === 'string' && elementOptions === 'group') {
        	this.opts = $.extend({}, defaults, { rel: 'group' + i });
	        this.group = this.$element.data(pluginName + '-group');
        }
        else {
	        if (typeof elementOptions === 'string' && elementOptions) {
		        elementOptions = PX.plugins.list[pluginName].presets[elementOptions] || {};
	        }
	        if (typeof elementOptions === 'object') {
		        var opts  = $.extend({}, options, elementOptions); // element options -> plugin init options
				this.opts = $.extend({}, defaults, opts); // options -> defaults
	        	this.opts.params = this.$element.data(pluginName + '-params');
	        }
	        else {
		        this.opts = null;
	        }
        }
        
        // Go!
        this.init();
    };
    
    /*
     * Shortcut for Plugin object prototype
     */
    Plugin.fn = Plugin.prototype;
    
    /*
     * Initialization logic
     */
    Plugin.fn.init = function () {
        var that = this;
        
        // Not an anchor
        if (that.$element[0].nodeName.toLowerCase() !== 'a') {
	        that.$element.addClass('a');
        }
        
        // Params ?
        if (that.opts && that.opts.href && that.opts.params) {
	        that.opts.href += '?' + that.opts.params;
        }
        
        // Group
        if (that.group) {
        	var group = '';
	        for (var i = 0, l = that.group.list.length; i < l; i++) {
		        group += '<a href="' + that.group.list[i] + '" class="hidden"></a>'
	        }
	        that.$group = $(group).insertAfter(that.$element).colorbox(that.opts);
        }
        
        // Events init
        that.events();
    };
    
    /*
     * Evenements
     */
    Plugin.fn.events = function () {
        var that = this;
        if (that.group) {
	        that.$element.on('click', function (e) {
	        	that.$group.filter(':eq(0)').trigger('click');
	        });
        }
        else if (that.opts) {
        	that.$element.on('click', function (e) {
				e.preventDefault();
				$.colorbox(that.opts);
				$[pluginName].data = {
					element: that.$element
				};
			});
        }
        else {
	        that.$element.colorbox(defaults);
        }
    };
    
    // Plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function (i) {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
            }
        });
    };

    /*
     * Mapping w/ colorbox
     */
    $[pluginName] = $.colorbox;
    
    /*
     * Presets
     */
    $[pluginName].presets = {};
    
    /*
     * Data
     */
    $[pluginName].data = {
        element: null
    };

}(jQuery));
