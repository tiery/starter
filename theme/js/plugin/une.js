<!--#include virtual="../libraries/jquery-easing.js"-->

(function ($) {

    'use strict';

    /*
     * Params
     */
    var pluginName = 'pxune',
        defaults = {
	        autoPlay: true
        },
        Plugin;

    /*
     * Plugin constructor
     */
    Plugin = function (element, options, i) {

        // Plugin exposed to window
        /* window[pluginName + i] = this; */

        // Main element
        this.$element = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');

        // Options
        var elementOptions = this.$element.data(pluginName);
        var opts = $.extend({}, options, elementOptions); // element options -> plugin init options
        this.opts = $.extend({}, defaults, opts); // options -> default

        // DOM elements
        this.$inner     = $('.une__inner', element);
        this.$wrapper   = $('.une__wrapper', element);
        this.$slider    = $('.une__slider', element);
        this.$elements  = $('.une__element', element);
        this.$dots      = $('<div class="une__ctrl-dots"></div>');
        this.$ctrlLeft  = $('<div class="une__ctrl-left"><span></span></div>');
        this.$ctrlRight = $('<div class="une__ctrl-right"><span></span></div>');
        
        // Properties
        this.status = 'wait';
        this.count = this.$elements.length;
        this.delta = 0;
        this.isPlay = false;
        this.timer = null;

        // Go!
        this.init();
    };

    /*
     * Shortcut for Plugin object prototype
     */
    Plugin.fn = Plugin.prototype;

    /*
     * Initialization logic
     */
    Plugin.fn.init = function () {
        var that = this;
        
        // Insert Crtl elements
        that.$inner.append(that.$ctrlLeft, that.$ctrlRight);
        
        // Create dots
        var dots = [];
        for (var i = 0, l = that.count; i < l; i++) {
	        dots.push('<div class="une__ctrl-dot"></div>');
        }
        that.$dots.append(dots.join('')).find(':eq(0)').addClass('active');
        that.$inner.append(that.$dots);
        
        // init layout
        that.layout();
        
        // remove hidden classes
        that.$elements.removeClass('visuallyhidden').each(function (i, el) {
	        $(this).attr('data-index', i);
        });
        
        // FadeOut text bloc
        that.$elements.filter(':gt(0)').find('.une__bloc').fadeOut();
        
        // place le dernier element en premier
        that.$elements.filter(':last-child').prependTo(that.$slider);
        
        // Décale le slider vers la gauche
        that.$slider.css('left', -that.delta);
        
        // Active les images
        that.$slider.find('[data-src]').each(function(){
	        var $this = $(this);
	        $this.attr('src', $this.attr('data-src'));
        });
				
        // Events init
        that.events();
        
        // Auto play ?
        if (that.opts.autoPlay && !PX.test('touch')) {
	        that.isPlay = true;
	        that.play();
        }
    };

    /*
     * Evenements
     */
    Plugin.fn.events = function () {
        var that = this;
        
        // User events
        that.$ctrlRight.on('click', function () {
        	that.process(-1);
        });
        
        that.$ctrlLeft.on('click', function () {
			that.process(1);
        });
        
        // Resize
        $(window).resize(function () {
	        that.layout();
        });
        
        // Stop/Play events
        that.$element.on('mouseover', function(){
	        that.stop();
        });
    };

    /*
     * Process
     */
    Plugin.fn.process = function (dir) {
        var that = this,
        	sign = (dir >= 0) ? '+' : '-',
        	currentIndex = 1,
        	nextIndex = 0,
        	$currentElement,
        	$nextElement,
        	$moveElement,
        	action;
        
        if (that.status == 'busy') {
	        return;
        }
        	
        that.status = 'busy';
        
        $currentElement = that.$elements.filter(':nth-child(2)');
        $nextElement = (dir >= 0) ? that.$elements.filter(':nth-child(1)') : that.$elements.filter(':nth-child(3)');
        $moveElement = (dir >= 0) ? that.$elements.filter(':last-child') : that.$elements.filter(':first-child');
        action = (dir >= 0) ? 'prepend' : 'append';
        nextIndex = $nextElement.attr('data-index');
		that.$dots.find('span').removeClass('active').filter(':eq(' + nextIndex + ')').addClass('active');
        
        $currentElement.find('.une__bloc').fadeOut(function () {
	        that.$slider.animate({ left: sign + '=' + that.delta }, 450, function () {
	        	$moveElement[action + 'To'](that.$slider);
	        	that.$slider.css('left', -that.delta);
	        	$nextElement.find('.une__bloc').fadeIn();
	        	that.status = 'wait';
		        if (that.isPlay) {
		        	that.player();
		        }
	        });
        });        
    };
    
    /*
     * Play
     */
    Plugin.fn.play = function () {
    	var that = this;
    	that.isPlay = true;
    	that.player();
    };
    
    /*
     * Stop
     */
    Plugin.fn.stop = function () {
    	var that = this;
    	that.isPlay = false;
    	clearTimeout(that.timer);
    };
    
    /*
     * Player process
     */
    Plugin.fn.player = function () {
    	var that = this;
	    that.timer = setTimeout(function () {
    		clearTimeout(that.timer);
	    	that.process(-1);
    	}, 3000);
    };
    
    /*
     * Layout calcul
     */
    Plugin.fn.layout = function () {
        var that = this,
        	sliderWidth;
        
        that.delta = that.$wrapper.width();
        that.$elements.width(that.delta);
        sliderWidth = that.count * that.delta;
        that.$slider.width(sliderWidth).css('left', -that.delta);
    };

    // Plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function (i) {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
            }
        });
    };

}(jQuery));