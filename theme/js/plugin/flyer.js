<!--#include virtual="../libraries/jquery.ba-throttle-debounce.min.js"-->
(function ($) {

	'use strict';

	/*
	 * Params
	 */
	var pluginName = 'flyer',
		defaults = {
			delta_top: 10,
			delta_bot: 0,
			support_only: true
		},
		support_sticky = PX.feature('position', 'sticky'),
		Plugin;

	/*
	 * Plugin constructor
	 */
	Plugin = function (element, options, i) {

		// Plugin exposed to window
		/* window[pluginName + i] = this; */
		this.$element = $(element);

		// Options
		var elementOptions = this.$element.data(pluginName);
		var opts = $.extend({}, options, elementOptions); // element options -> plugin init options
		this.opts = $.extend({}, defaults, opts); // options -> default
		
		// DOM elements
		this.$win = $(window);
		this.$track = this.$element.parent();
		
		// Go!
		this.init();
	};

	/*
	 * Shortcut for Plugin object prototype
	 */
	Plugin.fn = Plugin.prototype;
	
	/*
	 * Initialization logic
	 */
	Plugin.fn.init = function () {
		var that = this;
		that.update();
		if (that.$win.height() > that.flyer_height && that.track_height > that.flyer_height) {
			if (support_sticky && that.$track.css('display') !== 'table-cell') {
				this.$element.addClass('flyer--sticky');
			}
			else if (!that.opts.support_only) {
				that.events();
			}
		}
	};
	
	Plugin.fn.update = function () {
		var that = this;
		that.track_height = that.$track.height();
		that.flyer_height = that.$element.height();
		that.min = that.$track.offset().top - that.opts.delta_top;
		that.max = that.min + that.track_height - that.flyer_height - that.opts.delta_bot;
	};
	
	Plugin.fn.process = function () {
		var that = this,
			userScroll = that.$win.scrollTop();
		if (userScroll > that.min){
			that.$element.css({'position': 'fixed', 'top': that.opts.delta_top, 'bottom': 'auto'});
			if (userScroll > that.max) {
				that.$element.css({'position': 'absolute', 'top': 'auto', 'bottom': that.opts.delta_bot});
			}
		}
		else {
			that.$element.css({'position': 'static', 'top': 'auto', 'bottom': 'auto'});
		}
	};

	/*
	 * Evenements
	 */
	Plugin.fn.events = function () {
		var that = this;
		
		// Bind Resize
		that.$win.resize($.throttle(250, function () {
			that.update();
		})).trigger('resize');
		
		// Bind Scroll
		that.$win.on('scroll.' + pluginName, function(){
			that.process();
		}).trigger('scroll.' + pluginName);
		
		// Update trigger
		that.$element.on('update.' + pluginName, function () {
			that.update();
			that.process();
		});
	};

	// Plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function (options) {
		return this.each(function (i) {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
			}
		});
	};

}(jQuery));