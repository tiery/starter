<!--#include virtual="preloader.js"-->
/*
 * Slideshow plugin
 */
(function (PX, $) {

    'use strict';

    /*
     * Params
     */
    var pluginName = 'pxSlideshow',
        dataAttribute = 'data-slideshow',
        defaults = {
          adCounter: 0,
          sCurrent: 'current',
          sNoPhoto: 'no-photo',
          adSelector: '#ad-site-rectangle-dst iframe.ad-iframe',
          transitionSpeed: 300,
          animeSlider: PX.test('rwd', ['desktop', 'tablet'])
        },
        _selector = function (val, pre) {
          pre = (!pre) ? '' : '^';
          return '[ ' + dataAttribute + pre +'="' + val + '"]';
        };
        
    /*
     * Plugin constructor
     */
    var Plugin = function (element, options, i) {
        
        // Plugin exposed to window
        window[pluginName + i] = this;
        
        // Main element
        this.$element    = $(element).attr('data-' + pluginName + '-id', pluginName + i).addClass(pluginName.toLowerCase() + '-init');
        
        // Options
        var opts  = $.extend({}, options, this.$element.data(pluginName + '-options')); // element options -> plugin init options
        this.opts = $.extend({}, defaults, opts); // options -> defaultChecked
        
        // DOM elements
        this.$current    = this.$element.find(_selector('current'));
        this.$before     = this.$element.find(_selector('before'));
        this.$after      = this.$element.find(_selector('after'));
        this.$main       = this.$element.find(_selector('main'));
        this.$photoWrap  = this.$element.find(_selector('photo-wrap'));
        this.$photo      = this.$element.find(_selector('photo'));
        this.$contents   = this.$element.find(_selector('content',1));
        this.$sliderWrap = this.$element.find(_selector('slider-wrap'));
        this.$slider     = this.$element.find(_selector('slider'));
        this.$items      = this.$slider.find('li');
                
        // Properties
        this.datas    = window['slideshow' + this.$element.attr('data-slideshow-id')];
        this.status   = 'wait'; // wait | busy
        this.lastItem = this.$items.length - 1;
        this.density  = (PX.test('retina')) ? 'retina' : 'normal',
        this.current  = 0;
        this.operator = 1; // -1 => before | 1 => after
        this.counter  = 1; // click counter before display ad
        this.delta    = this.$items.filter(':first').width(); // delta for slider
        
        // Go!
        this.init();
    };
    
    /*
     * Shortcut for Plugin object prototype
     */
    Plugin.fn = Plugin.prototype;
    
    /*
     * Initialization logic
     */
    Plugin.fn.init = function () {
        var that = this;
        
        // Si Retina, update first photo to its retina version
        if (that.density === 'retina') {
	        that.$photo.attr('src', that.datas[0].path.retina);
        }
        
        // Calculate slider width
        that.sliderWidth();
        
        // Events init
        that.events();
    };
    
    /*
     * Photos preloader
     */
    Plugin.fn.preload = function () {
        var that = this,
            loader,
            photos = [],
            i = 1, // on ne prÃ©charge pas la 1ere photo car dÃ©jÃ  appelÃ©e dans le HTML
            l = that.datas.length;
            
        for (; i < l; i++) {
			that.datas[i].path && photos.push(that.datas[i].path[that.density]);
        }
        loader = new PX.Preloader(photos);
        loader.init();
    };
    
    /*
     * Calcul slider width
     */
    Plugin.fn.sliderWidth = function () {
        var that = this,
            $last = that.$items.filter(':last');
        
        that.$slider.width(that.$items.length * $last.width());
    };
    
    /*
     * Evenements
     */
    Plugin.fn.events = function () {
        var that = this,
            beforeAction = function () {
              that.operator = -1;
              that.preProcess();
            },
            afterAction = function () {
              that.operator = 1;
              that.preProcess();
            };
        
        // After action
        that.$after.click(afterAction);
        
        // Before action
        that.$before.click(beforeAction);
        
        // Item action
        that.$items.click(function afterAction(){
          that.itemProcess(this);
        });
        
        // Keys action
        $(document).on('keyup', function(e){
	        if (e.which === 37) {
		        beforeAction();
	        }
	        else if (e.which === 39) {
		        afterAction();
	        }
        });
        
        // Preload photos on load
        $(function(){
	        that.preload();
        });
    };
    
    /*
     * Relayout
     */
    Plugin.fn.relayout = function () {
        var that = this;
        // Slider properties
    };
    
    /*
     * Pre process
     */
    Plugin.fn.preProcess = function () {
        var that = this,
            modulo;
        
        // Check/Update slideshow status
        if (that.status === 'busy') {
	        return;
        }
        that.status = 'busy';
        
        // Check display type (default or ad) via counter
        modulo = that.counter % that.opts.adCounter;
        if (modulo === 0 && that.counter > 0) {
          that.processAd();
        }
        else {
          that.process();
        }
    };
    
    /*
     * Process Ad
     */
    Plugin.fn.processAd = function () {
        var that = this;
        that.postProcess();
    };
    
    /*
     * Process default
     */
    Plugin.fn.process = function () {
        var that = this,
            nextDatas,
            next;
        
        // Check next index
        if (that.current === 0 && that.operator === -1) {
          next = that.lastItem;
        }
        else if (that.current === that.lastItem && that.operator === 1) {
          next = 0;
        }
        else {
          next = that.current + that.operator * 1;
        }
        
        that.update(next);
    };
    
    /*
     * Item process
     */
    Plugin.fn.itemProcess = function (handler) {
	    var that = this,
	    		next = that.$items.index($(handler));
	    
	    if (next !== that.current) {
		    that.update(next);
	    }
    };
    
    /*
     * Update
     */
    Plugin.fn.update = function (next) {
    	var that = this,
    	    nextDatas;
    	
    	// Get next photo datas
      nextDatas = that.datas[next];
      
      // Update current number
      that.$current.html(next + 1);
              
      // Update text contents (photos, texts,...)
      that.updContents(nextDatas.content);
      
      // Update slider
      that.updSlider(next);
      
      // Update photo
	  that.updPhoto(nextDatas, function updPhotoCallback() {
		  // Update market stuff
		  that.updMarket();
		  // Update slideshow current
    	  that.current = next;
		  // Post process
		  that.postProcess();
	  });
	  
	  
    };
    
    /*
     * Update contents
     */
    Plugin.fn.updContents = function (data) {
        var that = this,
            content;
        
        // Update contents
        for (content in data) {
			that.$contents.filter('[data-slideshow="content-' + content + '"]').html(data[content]);
        }  
    };
    
    /*
     * Update slider
     */
    Plugin.fn.updSlider = function (next) {
        // Uniquement en desktop
        if (PX.test('rwd', 'desktop')) {
            var that = this,
                oldCurrent = 0,
                newCurrent = 0,
            		sCurrent = that.opts.sCurrent;
            
            oldCurrent = that.$slider.find('li').index(that.$items.filter('.' + sCurrent));
            that.$items.removeClass(sCurrent).filter(':eq(' + next + ')').addClass(sCurrent);
            newCurrent = that.$slider.find('li').index(that.$items.filter('.' + sCurrent));
            that.slide(oldCurrent, newCurrent);
        }
    };
    
    /*
     * Slide
     */
    Plugin.fn.slide = function (current, next) {
    	var that = this;
        if (that.$sliderWrap.width() > that.$slider.width() || !that.opts.animeSlider) {
            return;
        }
    	if (next > current && next !== that.lastItem) {
    		that.$slider.animate({ marginLeft: -that.delta }, 400, function(){
	    		that.$slider
	    			.append(that.$slider.find('li:first'))
	    			.css('margin-left', 0);
    		});
    	}
    	else {
    		that.$slider
	    			.prepend(that.$slider.find('li:last'))
	    			.css('margin-left', -that.delta);
	    	that.$slider.animate({ marginLeft: 0 }, 400);
    	}
    };
    
    /*
     * Update photo
     */
    Plugin.fn.updPhoto = function (datas, callback) {
        var that = this,
            speed = that.opts.transitionSpeed,
            path = (datas.path) ? datas.path[that.density] : false,
            timer;
            
        // Photo présente
        if (path) {
	        //if (PX.test('rwd', 'desktop')) {
	        if (false) {
	            that.$photo.fadeTo(speed, 0, function(){
	    	        that.$photo.attr('src', path);
	    	        that.$main.removeClass(that.opts.sNoPhoto);
	    	        that.$photo.fadeTo(speed, 1, callback);
	            });
	        }
	        else {
	            that.$photo.attr('src', path);
	            that.$main.removeClass(that.opts.sNoPhoto);
	            callback();
	        }
		}
		
		// Pas de photo
		else {
			that.$main.addClass(that.opts.sNoPhoto);
			callback();
		}
    };
    
    /*
     * Update Market
     */
    Plugin.fn.updMarket = function () {
    	/*
    	var that = this,
            $el;
        
        // Ads
        $.each($(that.opts.adSelector), function(i, el){
        	$el = $(el);
	        $el.attr('src', $el.attr('src'));
        });
        
        // Tracking
        _gaq.push(['_trackPageview', window.location.pathname]);
        */
    };
    
    /*
     * Post process
     */
    Plugin.fn.postProcess = function () {
        var that = this;
        that.status = 'wait';
        that.counter++;
    };
    
    // Plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        return this.each(function (i) {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
            }
        });
    };

}(PX, jQuery));