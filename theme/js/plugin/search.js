<!--#include virtual="../libraries/jquery-highlight.js"-->
(function ($) {

	'use strict';

	/*
	 * Params
	 */
	var pluginName = 'search',
		defaults = {
			targetSelector: '#article_body'
		},
		Plugin;

	/*
	 * Plugin constructor
	 */
	Plugin = function (element, options, i) {

		// Plugin exposed to window
		/* window[pluginName + i] = this; */
		this.$element = $(element);

		// Options
		var elementOptions = this.$element.data(pluginName);
		var opts = $.extend({}, options, elementOptions); // element options -> plugin init options
		this.opts = $.extend({}, defaults, opts); // options -> default
		
		// DOM elements
		this.$target = $(this.opts.targetSelector);
		this.$input = this.$element.find('input');
		
		// Properties
		this.cache;
		this.highlightOptions = {
			className: 'search_highlight'
		};
		
		// Go!
		this.init();
	};

	/*
	 * Shortcut for Plugin object prototype
	 */
	Plugin.fn = Plugin.prototype;
	
	/*
	 * Initialization logic
	 */
	Plugin.fn.init = function () {
		var that = this;
		that.$results = $('<div></div>');
		that.$input.after(that.$results);
		that.events();
	};

	/*
	 * Evenements
	 */
	Plugin.fn.events = function () {
		var that = this;

		that.$input.on('keyup change', function(){
			var $this = $(this),
				val = $this.val();
			if (val.length < 3) {
				that.$target.removeHighlight();
				that.$results.empty();
				return;
			}
			that.$target
				.removeHighlight()
				.highlight(val);
			/*if (that.$target[0].highlight_counter) {
				that.$results.html(that.$target[0].highlight_counter + ' résultats');
			}*/
		});
		
		$(window).on('SWITCHER_OPEN', function(e, switcher){
			var $switcher = $(switcher);
			if ($switcher.hasClass('tool--search')) {
				that.$input.focus().trigger('keyup');
			}
		});
		
		$(window).on('SWITCHER_CLOSE', function(e, switcher){
			var $switcher = $(switcher);
			if ($switcher.hasClass('tool--search')) {
				that.$target.removeHighlight();
			}
		});
		
	};

	// Plugin wrapper around the constructor,
	// preventing against multiple instantiations
	$.fn[pluginName] = function (options) {
		return this.each(function (i) {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin(this, options, i));
			}
		});
	};

}(jQuery));