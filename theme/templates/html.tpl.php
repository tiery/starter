<!doctype html>
<!--[if lt IE 7 ]> <html lang="fr" class="no-js no-touch ie lte-ie9 lte-ie8 lte-ie7 lt-ie7"> <![endif]-->
<!--[if IE 7 ]>    <html lang="fr" class="no-js no-touch ie ie7 lte-ie9 lte-ie8 lte-ie7"> <![endif]-->
<!--[if IE 8 ]>    <html lang="fr" class="no-js no-touch ie ie8 lte-ie9 lte-ie8"> <![endif]-->
<!--[if IE 9 ]>    <html lang="fr" class="no-js no-touch ie ie9 lte-ie9"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html lang="fr" class="no-js no-touch"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<title><?php print $head_title; ?></title>
	<?php print $head; ?>
	<script>
	(function(ua,h,c) {
		h[c] = h[c].replace('no-js', 'js');
		var os = ['windows','macintosh','linux','android','iphone'];
		for (var i=0, l=os.length; i<l; i++) {
			if(new RegExp(os[i]).test(ua)){ h[c] += ' ' + os[i]; break; }
		}
	})(navigator.userAgent.toLowerCase(), document.documentElement, 'className');
	<?php
	$PX = array(
		'config' => array(
			'cacheQuery' => $flush_query,
			'pageUrl' => $GLOBALS['base_url'] . '/' . drupal_lookup_path('alias', current_path()),
			'themePath' => $GLOBALS['base_url'] . $directory . '/',
			'preprocessJS' => ($preprocess_js) ? true : false,
			'devMode' => false,
			'env' => (PXIsProd) ? 'prod' : 'preprod',
			'adsRegie' => 'openx'
		),
		'path' => array(
			'scripts' => '/' . $directory . '/js/'
		)
	);
	?>
	var PX = <?=json_encode($PX)?>;
	PX["ads"] = {};
	</script>
	<?php print $styles; ?>
	<meta name="viewport" content="width=device-width">
	<meta http-equiv="cleartype" content="on">
	<?php print $tag_ga; ?>
</head>
<body class="<?php print $classes; ?>" itemscope itemtype="http://schema.org/WebPage">
<?php print $page_top; ?>
<?php include($directory . '/includes/header.php'); ?>
<?php print $page; ?>
<?php include($directory . '/includes/footer.php'); ?>
<div id="scripts">
	<?php print $scripts; ?>
</div><!-- scripts -->
<?php print $page_bottom; ?>
</body>
</html>