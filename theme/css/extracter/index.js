/*
 * Extrait des styles destinés à être édités dans le BO Drupal
 * extracter (files, patterns[, options])
 * https://www.npmjs.org/package/css/
 */

/*
 * Required modules
 */
var util = require('util');
var fs = require('fs');
var merge = require('merge');
var css = require('css');

/*
 * Default options
 */
var defaults = {
	src: './css/masters/',
	dst: './css/customs/',
	compress: true
};

/*
 * Module
 */
function extracter (files, patterns, options) {
	// Errors handling
	if (!files) {
		util.error('"files" parameter missing.');
		return;
	}
	if (!patterns) {
		util.error('"patterns" parameter missing.');
		return;
	}
	// Merge option & create new Extraction object
	var params = merge.recursive(true, defaults, options);
	new Extraction (files, patterns, params);
}

module.exports = extracter;

/*
 * Constructor
 */
function Extraction (files, patterns, options) {
	this.files = files;
	this.patterns = patterns;
	this.o = options;
	this.init();
}

/*
 * Init
 */
Extraction.prototype.init = function () {
	var self = this,
		files = self.files;
	
	files.forEach(function(file){
		var filePath = self.o.src + file[0];
		fs.readFile(filePath, 'utf8', function(error, data){
			if (error) {
				util.error(error);
			}
			else {
				self.procss(file, data);
			}
		});
	});
};

/*
 * Procss
 * Processing each file styles
 */
Extraction.prototype.procss = function (file, styles) {
	var self = this,
		dst = self.o.dst + file[1],
		parse = css.parse(styles),
		rules = parse.stylesheet.rules,
		newSheet = {
			'type' : 'stylesheet',
			'stylesheet': {
				'rules': []
			}
		};
		
	// Iterate on each rule
	// and creating a new stylesheet if find matched patterns
	rules.forEach(function(rule){
		var newRule = {
			'type': 'rule',
			'selectors': rule.selectors,
			'declarations': []
		};
		if (rule.type === 'rule') {
			rule.declarations.forEach(function(declaration) {
				self.patterns.forEach(function(pattern){
					var reg = new RegExp(pattern[0], 'g');
					if (reg.test(declaration.value)) {
						declaration.value = declaration.value.replace(reg, pattern[1]);
						newRule.declarations.push(declaration);
					}
				});
			});
		}
		// Matched declarations have been detected
		if (newRule.declarations.length) {
			newSheet.stylesheet.rules.push(newRule);
		}
	});
	
	// New rules have been added
	if (newSheet.stylesheet.rules.length) {
		newSheet = css.stringify(newSheet, { compress: self.o.compress });
		fs.writeFile(dst, newSheet, function(err) {
			if (err) {
				console.log(err);
	    }
	    else {
				console.log('The file ' + dst + ' was saved!');
	    }
		});
	}
	else {
		console.log('No styles have been extracted.');
	}
}